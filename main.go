	package main
	
	import (
		"log"
		"net/http"
		"html/template"
	)
	var index = template.Must(template.ParseFiles("templates/index.html"))
	func main() {
		fs := http.FileServer(http.Dir("static/"))
		helloHandler := func(w http.ResponseWriter, req *http.Request) {
			index.Execute(w, nil)
		}
		http.Handle("/static/", http.StripPrefix("/static/", fs))
		http.HandleFunc("/", helloHandler)
		log.Fatal(http.ListenAndServe(":8080", nil))
	}